package com.atlassian.http.router;

import static org.hamcrest.core.Is.is;

import com.atlassian.http.router.internal.PathMatcher;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestParser {
    @Test
    public void testEmptyPath() throws Exception {
        PathMatcher m = PathMatcher.parse("/");
        assertTrue(m.matches("/"));
        assertThat(m.getParams().size(), is(0));
    }

    @Test
    public void testSingleWordPath() throws Exception {
        PathMatcher m = PathMatcher.parse("/word");
        assertTrue(m.matches("/word"));
        assertThat(m.getParams().size(), is(0));
    }

    @Test
    public void testSingleParamPath() throws Exception {
        PathMatcher m = PathMatcher.parse("/:sandwich");
        assertTrue(m.matches("/cat"));
        assertThat(m.getParams().size(), is(1));
        assertThat(m.getParams().containsKey("sandwich"), is(true));
        assertThat(m.getParams().get("sandwich"), is("cat"));
    }

    @Test
    public void testStaticPaths() throws Exception {
        PathMatcher m = PathMatcher.parse("/a/static/path");
        assertTrue(m.matches("/a/static/path"));
        assertThat(m.getParams().size(), is(0));
        assertTrue(m.matches("/a/static/path/"));

        assertFalse(m.matches("/b/static/path"));
        assertFalse(m.matches("/a/static/pat"));
        assertFalse(m.matches("/a/static/path/extention"));
        assertFalse(m.matches("/a/static/route/path/"));
    }

    @Test
    public void testPathsWithParams() throws Exception {
        PathMatcher m = PathMatcher.parse("/a/:path/to/a/thing");

        assertTrue(m.matches("/a/path/to/a/thing"));
        assertTrue(m.getParams().containsKey("path"));
        assertThat(m.getParams().get("path"), is("path"));

        assertTrue(m.matches("/a/cat/to/a/thing"));
        assertTrue(m.getParams().containsKey("path"));
        assertThat(m.getParams().get("path"), is("cat"));

        assertTrue(m.matches("/a/way/to/a/thing"));
        assertTrue(m.getParams().containsKey("path"));
        assertThat(m.getParams().get("path"), is("way"));

        assertTrue(m.matches("/a/:path/to/a/thing"));
        assertTrue(m.getParams().containsKey("path"));
        assertThat(m.getParams().get("path"), is(":path"));
    }

    @Test
    public void testPathsWithMultipleParams() throws Exception {
        PathMatcher m = PathMatcher.parse("/a/:path/:to/a/thing");

        assertTrue(m.matches("/a/path/to/a/thing"));
        assertTrue(m.getParams().containsKey("path"));
        assertThat(m.getParams().get("path"), is("path"));
        assertThat(m.getParams().get("to"), is("to"));

        assertTrue(m.matches("/a/cat/to/a/thing"));
        assertTrue(m.getParams().containsKey("path"));
        assertThat(m.getParams().get("path"), is("cat"));
        assertThat(m.getParams().get("to"), is("to"));

        assertTrue(m.matches("/a/way/around/a/thing"));
        assertTrue(m.getParams().containsKey("path"));
        assertThat(m.getParams().get("path"), is("way"));
        assertThat(m.getParams().get("to"), is("around"));

        assertTrue(m.matches("/a/:path/:to/a/thing"));
        assertTrue(m.getParams().containsKey("path"));
        assertThat(m.getParams().get("path"), is(":path"));
        assertThat(m.getParams().get("to"), is(":to"));
    }

    @Test
    public void testTrailingPath() throws Exception {
        PathMatcher m = PathMatcher.parse("/a/path/to/a/*thing");

        assertTrue(m.matches("/a/path/to/a/thing"));
        assertThat(m.getParams().get("thing"), is("thing"));

        assertTrue(m.matches("/a/path/to/a/things/set"));
        assertThat(m.getParams().get("thing"), is("things/set"));

        assertTrue(m.matches("/a/path/to/a/bag/o/stuff"));
        assertThat(m.getParams().get("thing"), is("bag/o/stuff"));
    }

}
