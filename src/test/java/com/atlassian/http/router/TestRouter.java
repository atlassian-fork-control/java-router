package com.atlassian.http.router;

import com.atlassian.http.Context;
import com.atlassian.http.Servlet;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestRouter {

    @Test
    public void test() throws Exception {
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse resp = mock(HttpServletResponse.class);

        when(req.getAttribute(eq(Context.class.getName()))).thenReturn(new Context());
        when(req.getPathInfo()).thenReturn("/road/to/perdition");
        when(req.getMethod()).thenReturn("GET");

        Router r = new Router();
        Hit hit = createTarget();
        r.
                register(Router.Method.GET, "/", createMiss()).
                register(Router.Method.GET, "/friends", createMiss()).
                register(Router.Method.GET, "/:cause/reasons", createMiss()).
                register(Router.Method.GET, "/:path/to/:thing", hit).
                register(Router.Method.GET, "/:never/to/:beChecked", createMiss());

        r.handle(req, resp);
        assertTrue(hit.hit);
    }


    private Hit createTarget() {
        return new Hit();
    }

    private Servlet createMiss() {
        return (HttpServletRequest req, HttpServletResponse resp) -> {
            throw new RuntimeException("Miss!");
        };
    }

    static class Hit implements Servlet {
        boolean hit = false;

        public void handle(HttpServletRequest req, HttpServletResponse resp) {
            hit = true;
        }
    }

}
