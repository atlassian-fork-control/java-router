package com.atlassian.http;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ServletFilterChain {

    public static ServletFilterChain defaultFitlers() {
        return of(ErrorLogger.filter)
                .around(AccessLogger.filter);
    }

    public static ServletFilterChain of(ServletFilter filter) {
        LinkedList<ServletFilter> list = new LinkedList<ServletFilter>();
        list.add(filter);
        return new ServletFilterChain(Collections.unmodifiableList(list));
    }

    private final List<ServletFilter> filters;

    private ServletFilterChain(List<ServletFilter> filters) {
        this.filters = filters;
    }

    public ServletFilterChain around(ServletFilter filter) {
        LinkedList<ServletFilter> list = new LinkedList<ServletFilter>();
        list.addAll(filters);
        list.add(filter);
        return new ServletFilterChain(
                Collections.unmodifiableList(list)
        );
    }

    public ServletFilter build() {
        ServletFilter accumulator = supplier -> supplier;
        for (ServletFilter filter : filters) {
            final ServletFilter acc = accumulator;
            accumulator = supplier -> acc.around(filter.around(supplier));
        }

        final ServletFilter filterSet = accumulator;
        return filterSet;
    }
}
