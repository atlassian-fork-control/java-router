package com.atlassian.http;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface Servlet {
    void handle(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
